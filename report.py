import os
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import load_structure
import cottonwood.toolbox as tb
from data import TrainingData
from cottonwood.experimental.visualize_structure import render


dpi = 300
reports_dir = "reports"
examples_dir = os.path.join(reports_dir, "examples")
filename_base = "example"
n_examples = 24

os.makedirs(examples_dir, exist_ok=True)

fig_width = 8
fig_height = 4.5

model_filename = os.path.join(reports_dir, "conv_enc_dec.pkl")
model = load_structure(model_filename)
model.add(TrainingData(), "training_data")
model.connect("training_data", "original", i_port_tail=0)

render(model)
tb.summarize(model)

for i_example in range(n_examples):
    model.forward_pass()
    image = model.blocks["training_data"].forward_out[0]
    normalized, reconstructed = model.blocks["loss"].forward_in

    def reconstitute(img):
        bounded = np.maximum(0, np.minimum(255, (img + .5) * 256))
        return bounded.astype(int)

    fig = plt.figure(figsize=(fig_width, fig_height))
    ax_image = fig.add_axes((
        .5 / fig_width, 1 / fig_height, 2 / fig_width, 2 / fig_height))
    ax_image.imshow(reconstitute(image))
    ax_norm = fig.add_axes((
        3 / fig_width, 1 / fig_height, 2 / fig_width, 2 / fig_height))
    ax_norm.imshow(reconstitute(normalized))
    ax_recon = fig.add_axes((
        5.5 / fig_width, 1 / fig_height, 2 / fig_width, 2 / fig_height))
    ax_recon.imshow(reconstitute(reconstructed))

    filename = os.path.join(
        examples_dir, f"{filename_base}_{100 + i_example}.png")
    plt.savefig(filename, dpi=dpi)
    plt.close()
