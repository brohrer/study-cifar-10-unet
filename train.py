from copy import deepcopy
import os
from cottonwood.activation import TanH as Activation
from cottonwood.conv2d import Conv2D
from cottonwood.loggers import ValueLogger
from cottonwood.loss import AbsoluteLoss
from cottonwood.operations import Copy
from cottonwood.operations_2d import CropUniform as Crop
from cottonwood.operations_2d import UpsampleDouble
from cottonwood.optimization import Adam
from cottonwood.pooling import MaxPool2D
from cottonwood.experimental.sparsify import SparsifyByChannel2D as Sparsify
from cottonwood.structure import load_structure, save_structure, Structure
import cottonwood.toolbox as tb
from data import TrainingData
from cottonwood.experimental.visualize_structure import render


reports_dir = "reports"
n_iter_train = int(1e7)
n_iter_report = int(1e2)
n_iter_save = int(1e3)

kernel_size = 3
learning_rate = 3e-6
n_active_channels = 1
n_crop_original = 8
n_kernels = 32
n_input_channels = 3
n_output_pixels = (32 - 2 * n_crop_original) * (32 - 2 * n_crop_original)

model_filename = os.path.join(reports_dir, "conv_enc_dec.pkl")


def main():
    try:
        model = retrieve_model()
    except Exception:
        model = create_model()

    train_model_w_checkpoints(model)


def retrieve_model():
    model = load_structure(model_filename)
    model.add(TrainingData(), "training_data")
    model.connect("training_data", "original", i_port_tail=0)
    return model


def create_model():
    optimizer = Adam(learning_rate=learning_rate)

    # First build two sizes of convolution blocks, each consisting of
    # convolution, normalization, a learned bias, and a nonlinearity.
    # The only difference between the two sizes is the number of kernels.
    conv_0 = Structure()
    conv_0.add(Conv2D(
        kernel_size=kernel_size,
        n_kernels=n_kernels,
        optimizer=optimizer,
    ), "conv")
    conv_0.add(Activation(), "activation")
    conv_0.connect_sequence(["conv", "activation"])

    conv_1 = Structure()
    conv_1.add(Conv2D(
        kernel_size=kernel_size,
        n_kernels=2 * n_kernels,
        optimizer=optimizer,
    ), "conv")
    conv_1.add(Activation(), "activation")
    conv_1.connect_sequence(["conv", "activation"])

    model = Structure()

    # Handle the incoming image data
    model.add(TrainingData(), "training_data")
    model.add(Copy(), "original")
    model.add(Crop(n_crop_original), "crop_original")

    # Create the top level (level 0) of convolution blocks
    # on the descending side
    model.add(deepcopy(conv_0), "conv_0_0")
    model.add(deepcopy(conv_0), "conv_0_1")

    # Create the level 1 convolution blocks
    model.add(MaxPool2D(window=2, stride=2), "max_pool")
    model.add(deepcopy(conv_1), "conv_1_0")
    model.add(Sparsify(n_active_channels=n_active_channels), "sparsify")
    # model.add(Sparsify(fraction=sparsify_fraction), "sparsify")
    model.add(deepcopy(conv_1), "conv_1_1")
    model.add(UpsampleDouble(), "upsample")

    # Create the level 0 convolution blocks
    # on the ascending side
    model.add(deepcopy(conv_0), "conv_0_2")
    model.add(Conv2D(
        kernel_size=kernel_size,
        n_kernels=n_input_channels,
        optimizer=optimizer,
    ), "conv_0_3")

    model.add(AbsoluteLoss(), "loss")

    model.connect_sequence([
        "training_data",
        "original",
        "conv_0_0",
        "conv_0_1",
        "max_pool",
        "conv_1_0",
        "sparsify",
        "conv_1_1",
        "upsample",
        "conv_0_2",
        "conv_0_3",
        "loss",
    ])

    model.connect("original", "crop_original", i_port_tail=1)
    model.connect("crop_original", "loss", i_port_head=1)

    return model


def train_model_w_checkpoints(model):
    os.makedirs(reports_dir, exist_ok=True)

    training_loss_logger = ValueLogger(
        n_iter_report=n_iter_report,
        reports_dir=reports_dir,
        value_name="training_loss")

    best_loss = 1e10
    best_model = None
    for i_iter in range(n_iter_train):
        model.forward_pass()
        model.backward_pass()
        training_loss_logger.log_value(
            model.blocks["loss"].forward_out /
            (n_input_channels * n_output_pixels))
        if (i_iter + 1) % n_iter_save == 0:
            current_loss = training_loss_logger.get_recent_values(
                n_recent_values=n_iter_save)
            if current_loss < best_loss:
                best_loss = current_loss
                model_to_save = deepcopy(model)
                model_to_save.remove("training_data")
                save_structure(model_filename, model_to_save)
                print(
                    f"Best loss so far is {best_loss} " +
                    f"at {i_iter + 1} iterations")


if __name__ == "__main__":
    main()
