# Convolutional encoder-decoder for CIFAR-10

This study is a demonstration of a fully convolutional encoder-decoder architecture.

### Installation

Get all of these files by navigating at the command line to the directory
where you want them to to live and cloning the repository:

    git clone https://gitlab.com/brohrer/study-cifar-10-unet.git
    cd study-cifar-10-unet

#### Get the CIFAR-10 data set

CIFAR-10 is a collection of labeled images from ten different classes,
including cats and dogs, airplanes and frogs. There is a
[data description here](https://www.cs.toronto.edu/~kriz/cifar.html)
and history of state-of-the-art results in 
[the Wikipedia article](https://en.wikipedia.org/wiki/CIFAR-10).

The complete CIFAR-10 data set is included in this repository,
but if you wanted to get if for yourself, here's what you'd do:
To download CIFAR-10, use the link from 
[the description page](https://www.cs.toronto.edu/~kriz/cifar.html)
for the Python version to get a 186 MB gzipped tarball. Extract it to
get a directory of images called `cifar-10-batches-py` and make sure that
directory is moved here, into the same directory as `train.py`.

The dataset is wrapped in a Cottonwood [block](
https://gitlab.com/brohrer/study-cifar-10-unet/-/blob/main/data.py)
that loads the images and labels
from the files, properly formats them, and feeds them, one per iteration,
to the classifier as it runs through its training and testing loops.

#### Install Cottonwood

This classifier was created in [the Cottonwood machine learning framework](
https://e2eml.school/cottonwood). The recommended way to install
Cottonwood is from the git repository. At the command line:

    git clone https://gitlab.com/brohrer/cottonwood.git
    python3 -m pip install -e cottonwood
    cd cottonwood
    git checkout v30

It's important to check out the "v30" branch. Cottonwood has no guarantees
of backward compatibility and it evolves quickly.

If you are new to Cottonwood, there's a pair of video walkthroughs of
the project [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24071959)
and [here](https://end-to-end-machine-learning.teachable.com/courses/322-convolutional-neural-networks-in-two-dimensions/lectures/24080930).

### Get reports from the model

This study comes with a pre-trained model. To run some informative reports:

    python3 report.py

This should create some structure diagrams, a detailed summary of the model structure
and all its hyperparameters, and visualizations of image reconstructions.

### Train the model

If you would like to train the model for yourself from scratch, or better yet,
make some changes and try to improve on it, I strongly encourage you to
modify your `train.py` and run:

    python3 train.py

This will create a running plot of the training loss in the reports
directory so you can track its progress.

### Architecture

The encoder consists two convolution layers, a max pooling layer, another convolution layer, and a
[k-sparse layer](https://e2eml.school/k_sparse_layer.html). The decoder is the reverse -- a convolution layer,
an upsample layer, and a pair of convolution layers. The fine details can be read in `summary.txt` after running`report.py`.

![structure diagram](images/structure_diagram.png)


### Limitations

Because CIFAR-10 images are only 32 x 32 pixels, there's only so many convolution passes that can be done before the padding becomes the dominant source of information. Also, the course resolution makes it difficult to intuit how effectively the encoding compresses visual information. However it does serve as an excellent proof of concept for debugging. 

### Examples

Here are a few examples of the results. The complete image is on the left. The compressed version is in the middle. The cropped original, compensating for the shrinkage that comes from repeated convolutions, is on the right. These results show 92% compression, that is, the center image is represented with 1/12 of the parameters of the matching patch on the right. 

![](images/example_103.png)

![](images/example_105.png)

![](images/example_107.png)

![](images/example_109.png)

![](images/example_113.png)
